StoreMaster

Apis for creating for jobs to upload mutiple images for processing from difeerent stores 
Getting info of visits 

Work Environment for the Project 

    OS : Ubuntu18.04
    IDE : visual studio code
    frameWork : Django / django-rest
    libraries : Django-Q , pandas



Improvements : 
- Better Serializers for validations of data and models
- More parallelisation on downloading and processing part (Async operations)
- Better configuration for Django-Q and models
- Better structured Code Base eg. with different modules for each view and models etc.
- Dockerize 
- Write Tests For APIs


Instructions for running code :

Install Redis-
https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-redis-on-ubuntu-18-04

Create a python virtualenvironment 
activate your virtual environment

Then run following commands : 

    - pip install -r requirements.txt
    - cd pulseproject
    - python manage.py migrate

Open another terminal with virtualenv activated 

    #run cluster of django-q 

    - python manage.py qcluster

get back to previous terminal

    #run django server
    - python manage.py runserver

APIS link :
https://www.getpostman.com/collections/5abe19c525ad3f8b33cd


