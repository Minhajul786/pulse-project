from django.apps import AppConfig


class StoresapiConfig(AppConfig):
    name = 'storesApi'
