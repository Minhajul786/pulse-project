
from .models import Store
from pathlib import Path

def update_data_from_csv():

    stores_api = Path().cwd()
    df_stores = pd.read_csv(Path(str(stores_api)+"/pulseproject/storesApi/resources/StoreMasterAssignment.csv"),usecols=["AreaCode","StoreName","StoreID"])
    stores = []
    for row in df_stores.itertuples():
        print(row)
        stores.append(Store(storeId=row.storeID,area=row.AreaCode,name=row.StoreName))

    Store.objects.bulk_create(stores)
