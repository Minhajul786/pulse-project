# Generated by Django 3.1.3 on 2020-11-21 16:28

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Job',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField()),
                ('modified_at', models.DateTimeField()),
                ('status', models.CharField(max_length=60)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Store',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('storeId', models.CharField(max_length=60)),
                ('area', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Visit',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('visit_date', models.DateField()),
                ('perimeter', models.FloatField()),
                ('errors', models.JSONField()),
                ('Job', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='storesApi.job')),
                ('store', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='storesApi.store')),
            ],
        ),
    ]
