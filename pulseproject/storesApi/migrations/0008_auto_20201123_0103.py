# Generated by Django 3.1.3 on 2020-11-23 01:03

from django.db import migrations

# Generated by Django 3.1.3 on 2020-11-22 23:27

from django.db import migrations
from pathlib import Path



def update_data_from_csv(apps, schema_editor=None):
    import pandas as pd
    store = apps.get_model('storesApi', 'Store')
    stores_api = Path(__file__).parents[1]
    print("------------")
    print(stores_api)

    df_stores = pd.read_csv(Path(str(stores_api)+"/resources/StoreMasterAssignment.csv"),usecols=["AreaCode","StoreName","StoreID"])
    stores = []
    for row in df_stores.itertuples():
        print(row)
        stores.append(store(storeId=row.StoreID,area=row.AreaCode,name=row.StoreName))

    store.objects.bulk_create(stores)







class Migration(migrations.Migration):

    dependencies = [
        ('storesApi', '0007_auto_20201123_0103'),
    ]


    operations = [
        migrations.RunPython(update_data_from_csv),
    ]
    