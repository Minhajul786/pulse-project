# Create your models here.


from django.db import models
from django.utils import timezone

class TimeStamped(models.Model):
    created_at = models.DateTimeField(editable=True)
    modified_at = models.DateTimeField(editable=True)

    def save(self, *args, **kwargs):
        if not self.created_at:
            self.created_at = timezone.now()

        self.modified_at = timezone.now()
        return super(TimeStamped, self).save(*args, **kwargs)

    class Meta:
        abstract = True


class Job(TimeStamped):
    status = models.CharField(max_length=60)

    def __str__(self):
        return f"JobId {self.id} status {self.status}"

class Store(models.Model):
    storeId = models.CharField(max_length=60)
    area = models.IntegerField()
    name = models.CharField(max_length=128)

    def __str__(self):
        return f"{self.name} code {self.storeId} area {self.area}"

class Visit(models.Model):
    visit_date = models.DateTimeField()
    perimeter = models.FloatField()
    errors = models.JSONField(blank=True, null=True)
    Job = models.ForeignKey(Job, on_delete=models.CASCADE, related_name="visits")
    store = models.ForeignKey(Store, on_delete=models.CASCADE, related_name="visits", blank=True, null=True)




