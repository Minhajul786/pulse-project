from rest_framework import serializers

class VisitSerializer(serializers.Serializer):
    store_id = serializers.CharField(required=True, max_length=128)
    image_url = serializers.ListField(child=serializers.URLField(), min_length=1)
    visit_time = serializers.DateField(required=True)



class JobFormSerializer(serializers.Serializer):
    count = serializers.IntegerField(required=True, min_value=1)
    visits = serializers.ListField(child=VisitSerializer() , min_length=1 )

    def validate(self,attrs):
        # print(attrs)
        count = attrs['count']
        visits = attrs['visits']
        if int(count) != len(visits):
            data = {"error" : "Number of visit counts is not equal to visits"}
        else:  
            data = super(JobFormSerializer,self).validate(attrs)
        return data