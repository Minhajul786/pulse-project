from django_q.tasks import async_task
from time import sleep
from PIL import Image
import requests
from io import BytesIO

from .models import Job , Visit , Store



def download_image(url):
    print("====")

    print(url)

def process_visits_data(data):
    """
    Download images and processes them , saves data in respective models and update job
    """
    print("**********************")
    some_error = False
    visits = []
    job = Job.objects.get(id=data['job_id'])

    for v in data['visits']:
        perimeter = 0
        errors = []
        store_id = v["store_id"]
        try :
            store = Store.objects.get(storeId=store_id)
            
            for url in v["image_url"]:
                try :
                    response = requests.get(url)
                    img = Image.open(BytesIO(response.content))
                    sleep(5)
                except Exception as e:
                    errors.append({"store_id" : store_id , "error" : f"failed to download image from URL {url} "})
                    break
                width, height = img.size

                perimeter += width + height
        except Store.DoesNotExist as e:
            
            errors.append({"store_id" : store_id , "error" : f"store with code {store_id} doesn't exist"})
            store = None
        print("ERRORS")
        print(errors)
        visits.append(Visit(
            visit_date = v["visit_time"],
            perimeter = perimeter,
            errors =errors,
            Job = job,
            store =store
        ))

        if errors:
            some_error = True

    Visit.objects.bulk_create(visits)
        

    
    job.status = "Failed" if some_error else "Completed"
    
    job.save()
    print("------------------------------------------")
    print(data)
    return "success"



