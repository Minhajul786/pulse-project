from django.urls import include, path
from . import views


urlpatterns = [
    path('status/<jobId>/',views.job_info , name="status"),
    path('submit/', views.create_job, name="create_job"),
    path('visits/', views.get_visits, name='get_visits')
]