# from django.shortcuts import render

# Create your views here.

from rest_framework import status
from rest_framework.decorators import api_view, throttle_classes
from rest_framework.response import Response
from django_q.tasks import async_task

from django.db.models import Q
from .serializer import JobFormSerializer
from .models import Job, Visit, Store
from .tasks import process_visits_data


from datetime import datetime
@api_view(['GET'])
def job_info(request, jobId):
    # if request.method == 'POST':
    #     return Response({"message": "Got some data!", "data": request.data})
    res = {}
    try : 
        job = Job.objects.get(id=jobId)#.visits.exclude(errors=None) 
        if job.status == 'Completed' or job.status == "Pending":
            res.update( { "status" : job.status , "job_id" : jobId})
        elif job.status == "Failed":
            print(job.visits.all())
            errors = []
            for v in job.visits.exclude(errors=None):
                errors.append(v.errors)
            res.update( { "status" : job.status , "job_id" : jobId , "error" : errors})

    except Job.DoesNotExist as e:
        print(e)
        return Response({"error" : f"Job with id {jobId} doesn't exist"}, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        raise(e)
        return Response({"error" : "something went wrong"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    return Response(res)

@api_view(['POST'])
def create_job(request):
    res_data = {}
    form_data = None
    if request.method == 'POST':
        ser_data = JobFormSerializer(data = request.data)
        if ser_data.is_valid():
            form_data = ser_data.validated_data
            print(form_data)
            if "error" in form_data: 
                res_data = { "message": form_data}
                return Response(res_data , status=status.HTTP_400_BAD_REQUEST)

            
            
        else:
            res_data = {"data": ser_data.errors}
            return Response(res_data , status=status.HTTP_400_BAD_REQUEST)
        
        print(res_data)
       
        job = Job(status="pending")
        job.save()

        async_task(process_visits_data,
            {"job_id" : job.id , **form_data}
            )
        print(job)
        return Response({"message": "success" , "job_id" : job.id}, status=status.HTTP_201_CREATED)



@api_view(['GET'])
def get_visits(request):
    print(request.GET)
    error = False
    errors = []
    area = request.GET.get('area')
    storeId = request.GET.get('storeId')
    ## validation for start and end date
    try :
        startDate = datetime.strptime(request.GET.get('startDate'),"%Y-%m-%d") if 'startDate' in request.GET else None
        endDate = datetime.strptime(request.GET.get("endDate"),"%Y-%m-%d") if 'endDate' in request.GET else None
        if endDate < startDate:
            error = True
            errors.append(f"endDate cann't be before startDate")
    except ValueError as e:
        error = True
        errors.append(f"startDate and endDate in not correct format yyyy-mm-dd")
    
    ## validation for area and store start
    querry = []
    try:
        if area :
        
            areas = Store.objects.values_list('area',flat=True).distinct()
            print(list(areas))
            if int(area) in areas:
                querry.append(Q(store__area = area))
            else:
                error = True
                errors.append(f"area with code {area} doesn't exist")
        if storeId :
            try :
                store = Store.objects.get(storeId=storeId)
                querry.append(Q(store__storeId = storeId))
            except Store.DoesNotExist as e:
                error = True
                errors.append(f"store with code {storeId} doesn't exist")
                
    except Exception as e:
        raise(e)
    ## end 




    if not error:

        if startDate: 
            querry.append(Q(visit_date__gt = startDate))
        if endDate:
            querry.append(Q(visit_date__lt = endDate))
        print(querry)
        visits = Visit.objects.filter(*querry).select_related('store')
        response = {"results" : []}
        grouped_visits = {}

        #TODO Use serializer for formatting data into desired format
        for v in visits:
            print(v)
            if v.store:
                store_id = v.store.storeId
                if store_id in grouped_visits :
                    grouped_visits[store_id]["data"].append({'date' : v.visit_date , "perimeter": v.perimeter})
                else:
                    grouped_visits.update({
                        store_id : {
                            "area" : v.store.area,
                            "store_name" : v.store.name,
                            "data" : [{'date' : v.visit_date , "perimeter": v.perimeter}]
                        }
                    })
                    
                print(v)
        for k,v in grouped_visits.items():
            response['results'].append({ "store_id" : k , **v})
        
        return Response(response)

    else:
        return Response(errors , status=status.HTTP_400_BAD_REQUEST)




